# Changelog

All notable changes to this project will be documented in this file.

## [v1.0.1]

### Change
- Fixed information of plugin
## [v1.0.0](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/10df06cb1be2b53a5b1bd489fedc568d61c76270)

### New
- Init Plugin