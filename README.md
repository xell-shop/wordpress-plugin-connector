# Xell

Xell es un plugin que usa el plugin "Connect Woocommerce with your api" para conectar tu Sitio con Xell.

- [Installing](#installing)
- [Connect](#connect)

# Installing

1) Download file in [here](https://gitlab.com/xell-shop/wordpress-plugin-connector/-/archive/master/wordpress-plugin-connector-master.zip)

2) Go to Dashboard in Wordpress 

3) Go to Plugins > Add New > Upload Plugin 

![alt install](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/install.png)

4) Upload File > Active Plugin 

![alt active](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/active.png)

# Connect

1) Go to Dashboard in Wordpress 

2) Go to Xell and Connect

![alt connect](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/connect.png)


Copyright (C) 1989, 1991 Free Software Foundation, Inc.  
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.